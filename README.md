```
========================================================================

::::::::::::::    :::::::    :::::::    ::::::::::::::::       ::::::::  
    :+:    :+:    :+::+:+:   :+::+:+:   :+::+:       :+:      :+:    :+: 
    +:+    +:+    +:+:+:+:+  +:+:+:+:+  +:++:+       +:+      +:+        
    +#+    +#+    +:++#+ +:+ +#++#+ +:+ +#++#++:++#  +#+      +#++:++#++ 
    +#+    +#+    +#++#+  +#+#+#+#+  +#+#+#+#+       +#+             +#+ 
    #+#    #+#    #+##+#   #+#+##+#   #+#+##+#       #+#      #+#    #+# 
    ###     ######## ###    #######    ################################  

************************** Created by p1r ******************************

========================================================================

usage: tunnels -l <location> -p <local_port>

required arguments:
  -l , --location   Location, the end of the tunnel.

optional arguments:
  -p , --port       The sending local port. Default 1337
  -h, --help        Print help and exit

   Available locations:
        North America:
                (1) Virginia
                (2) Ohio
                (3) Oregon
                (4) California
        South America:
                (5) São Paulo
        Europe:
                (6) Dublin
                (7) London
                (8) Frankfurt
                (9) Paris
                (10) Milan
                (11) Stockholm
        Africa:
                (12) Cape Town
        Asia:
                (13) Singapore
                (14) Tokyo
                (15) Seoul
                (16) Bahrain
                (17) Hong Kong
                (18) Mumbai
        Oceania:
                (19) Sydney

example: tunnels -l 7
```

# tunnels
Without any other options a quick fix to watch netflix in another country, encrypt your local traffic or just because it's nice with SSH tunnels.

Specify an aws region to orchestration.sh and have a up and running connected SSH Server already configured in that region, in under a minute.
Just configure your local proxy settings to whatever port you gave as inparameter (or default 1080).

**Prerequisites**

*  You need an AWS account.
*  Inside that account an IAM User with programmatic access and this user policy:
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "cloudformation:CreateStack",
                "cloudformation:DeleteStack",
                "cloudformation:DescribeStacks",
                "ec2:DescribeInstances",
                "ec2:DescribeImages",
                "ec2:ImportKeyPair",
                "ec2:DeleteKeyPair",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:CreateSecurityGroup",
                "ec2:DescribeSecurityGroups",
                "ec2:DeleteSecurityGroup",
                "ec2:RunInstances",
                "ec2:TerminateInstances"
            ],
            "Resource": "*"
        }
    ]
}
```

*  You need awscli:
  
```bash
$ pip3 install awscli --upgrade --user
```

*   Next is to configure the awscli with the newly created user AWS access and secret key id, using
```bash
$ aws configure --profile tunnel-user
```
Notice that the region configured can be left default, also let the output be default (json).

You are now good to go.


**How to**

Coming..

