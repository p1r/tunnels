# pip install pyfiglet
# pip install colorama

# poison
# alligator
import sys
import colorama
from colorama import Fore, Style
import pyfiglet
from pyfiglet import Figlet
import argparse
import subprocess

# parser = argparse.ArgumentParser()
# parser._action_groups.pop()
# required = parser.add_argument_group('required arguments')
# optional = parser.add_argument_group('optional arguments')
availabilityZones = { 
    'Virginia': 'us-east-1',
    'Ohio': 'us-east-2',
    'Oregon': 'us-west-1', 
    'California': 'us-west-2',
    'São Paulo': 'sa-east-1',
    'Dublin': 'eu-west-1',
    'London': 'eu-west-2',
    'Frankfurt': 'eu-central-1',
    'Paris': 'eu-west-3',
    'Milan': 'eu-south-1',
    'Stockholm': 'eu-north-1',
    'Cape Town': 'af-south-1',
    'Singapore': 'ap-southeast-1',
    'Tokyo': 'ap-northeast-1',
    'Seoul': 'ap-northeast-2',
    'Bahrain': 'me-south-1',
    'Hong Kong': 'ap-east-1',
    'Mumbai': 'ap-south-1',
    'Sydney': 'ap-southeast-2' }

def returnLocations():
    hello = ""
    hello += "\n   Available locations:\n"
    count=0;
    for x in availabilityZones:
        if count == 0:
            hello += " \tNorth America:\n"
        elif count == 4:
            hello += " \tSouth America:\n"
        elif count == 5:
            hello +=" \tEurope:\n"
        elif count == 11:
            hello +=" \tAfrica:\n"
        elif count == 12:
            hello +=" \tAsia:\n"
        elif count == 18:
            hello +=" \tOceania:\n"
        hello += "\t\t" + "(" + str(count + 1) + ") "+ x + "\r\n"
        count+=1
    print(hello)
    return hello

def printHelpAndQuit():
    parser.print_help()
    returnLocations()
    print("example: tunnels -l 7\n")
    quit()
# required.add_argument('-r', '--region', help="Use --list to list all possible regions", action="store_true", required=True)

# optional.add_argument('-p', '--port', action="store_true")
# optional.add_argument('-l', '--list', action="store_true")
# parser._action_groups.append(optional) # added this line
# parser.parse_args()
# cookie=returnLocations()

clear_screen='\033[2J'
bold = '\033[1m'
custom_fig = Figlet(font='alligator3')
custom_fig2 = Figlet(font='poison')
print(clear_screen + Style.BRIGHT + Fore.GREEN + '========================================================================\n')
print(Fore.GREEN + Style.BRIGHT + custom_fig.renderText('tunnels'))
print(Fore.BLUE + bold + '************************** Created by p1r ******************************\n')
print(Fore.GREEN + Style.BRIGHT + '========================================================================\n')

parser = argparse.ArgumentParser(usage="tunnels -l <location> -p <local_port>", add_help=False)
required = parser.add_argument_group('required arguments')
optional = parser.add_argument_group('optional arguments')
required.add_argument("-l", "--location", action="store", metavar='', help="Location, the end of the tunnel."   )
optional.add_argument("-p", '--port', metavar='', help="The sending local port. Default 1337")
optional.add_argument('-h', '--help', help="Print help and exit", action='store_true')

location = ""
args = parser.parse_args()

if args.location:
    if args.location.isdigit() and int(args.location) < 20:
        location = availabilityZones.get(list(availabilityZones)[int(args.location)-1])
    else:
        for key, value in availabilityZones.items():
            if args.location.lower() == key.lower():
                location = value
        if location == "":
            printHelpAndQuit()
else:
    printHelpAndQuit()

subprocess.run(['bash', 'orchestration.sh', location])
# print("WHAAAAAAAAAAAAAAAAAAAT" + location)
# if args.list:
#     parser.print_help()
#     print('\nAvailable locations:')
#     count=0;
#     for x in availabilityZones:
#         if count == 0:
#             print(" North America:")
#         elif count == 4:
#             print(" South America:")
#         elif count == 5:
#             print(" Europe:")
#         elif count == 11:
#             print(" Africa:")
#         elif count == 12:
#             print(" Asia:")
#         elif count == 18:
#             print("Oceania:")
#         print("\t"+x)
#         count+=1
    
#     quit()

# custom_fig = Figlet(font='alligator3')
# custom_fig2 = Figlet(font='poison')
# print(Fore.GREEN + custom_fig.renderText('tunnels'))
# parser.parse_args()
# print(Fore.BLUE + custom_fig.renderText('through cloud'))
# print(Fore.BLUE + "Hello World")

# ascii_banner = pyfiglet.figlet_format("Hello!!")
# print(ascii_banner)