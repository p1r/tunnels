#!/bin/bash

# Run a validate and an menu program here through python

aws configure --profile tunnel-user set default.region $1

REGION="$1"
STACKNAME="tunnel-ec2-sg-$REGION-stack"
IP="$(curl ipinfo.io -s| jq -r .ip)/32"
KEYNAME="tunnelkey"
AMI="$(aws ec2 describe-images --owners 099720109477 --filters 'Name=name,Values=ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-????????' 'Name=state,Values=available' --output json | jq -r '.Images | sort_by(.CreationDate) | last(.[]).ImageId')"

tearDownTunnel(){
    echo "Tearing down tunnel infrastructure"
    pid=$(ps aux | grep -i "ssh -N -C -D 1080 -l ubuntu" | head -n1 | awk '{print $2}')
    kill -9 $pid 2> /dev/null
    wait $pid 2> /dev/null
    aws cloudformation delete-stack --stack-name $STACKNAME
    aws ec2 delete-key-pair --key-name $KEYNAME
    rm -f ~/.ssh/$KEYNAME*
    exit 0
}

echo "Creating key pair"
ssh-keygen -q -t rsa -b 2048 -f ~/.ssh/$KEYNAME -N "" > /dev/null
echo "Exporting public key"
aws ec2 import-key-pair --key-name "$KEYNAME" --public-key-material file://~/.ssh/$KEYNAME.pub > /dev/null
mv -f ~/.ssh/$KEYNAME ~/.ssh/$KEYNAME.pem # aws requires this naming format
chmod 400 ~/.ssh/$KEYNAME.pem

echo "Creating cloudformation stack"
aws cloudformation create-stack --parameters ParameterKey=Region,ParameterValue="$REGION"b \
ParameterKey=Ip,ParameterValue=$IP \
ParameterKey=KeyName,ParameterValue=$KEYNAME \
ParameterKey=Ami,ParameterValue=$AMI \
--stack-name $STACKNAME \
--template-body file://cfn-ec2-instance-sg.yml > /dev/null \
|| tearDownTunnel

while [ $(aws cloudformation describe-stacks --stack-name $STACKNAME | jq -r .Stacks[0].StackStatus) = 'CREATE_IN_PROGRESS' ]
do
    echo "Creating SSH Server..."
    sleep 5
done

if [ $(aws cloudformation describe-stacks --stack-name $STACKNAME | jq -r .Stacks[0].StackStatus)  = 'CREATE_COMPLETE' ]
then
    echo "Create complete"
else 
    echo "Something went wrong."
    tearDownTunnel
fi

# god this is ugly
REMOTE_IP=$(aws ec2 describe-instances --query 'Reservations[*].Instances[*].[PublicIpAddress]' --filters Name=instance-state-name,Values=running \
 | tr -d '\n\"[:space:][\]')

printf "Configuring service\nInitializing setup\n"
sleep 13 # If Connection refused then increase this sleep time 
#scp -i ~/.ssh/"$KEYNAME.pem" sshd_config ubuntu@$REMOTE_IP:/etc/ssh
ssh ubuntu@$REMOTE_IP -i ~/.ssh/$KEYNAME.pem -o StrictHostKeyChecking=no \
sudo service sshd restart

printf 'Server is all setup\nTunneling through port 1080\n'
ssh -N -C -D 1080 -l ubuntu -i ~/.ssh/$KEYNAME.pem $REMOTE_IP &

while [ "$input" != 'q' ];
do
    echo "Enter q to stop tunneling and tear down the tunnel."
    read input
done

tearDownTunnel


# What you do need is to set up an sshd_config and scp it to /etc/ssh
# Make a menu, list all availability-zones
# Remove everything if ROLLBACK
# Maybe make this (or also as) a python-script.
# Break out the deletion of stack and keys to a function, call it whenever something goes wrong
# When tunneling, let the user input q for stopping the tunneling and tear down infrastructure,

exit 0
