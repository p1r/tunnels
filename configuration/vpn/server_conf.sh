#!/bin/sh

sudo apt-get update
sudo apt-get openssl
mkdir /etc/openvpn
cd /etc/openvpn
wget https://github.com/OpenVPN/easy-rsa/archive/3.0.1.tar.gz
tar xzvf 3.0.1.tar.gz
rm 3.0.1.tar.gz

